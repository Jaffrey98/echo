import os.path as osp

def pardir(path, depth=1):
    """
    Returns path to parent directory.
    """
    path = osp.abspath(path)
    if not osp.exists(path):
        raise ValueError('{path} Directory does not exist.'.format(path=path))

    for i in range(depth):
        path = osp.dirname(path)

    return path