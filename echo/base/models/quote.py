from django.db            import models

from base.models.author   import Author
from base.models.category import Category
    
class Quote(models.Model):
    content    = models.TextField()
    author     = models.ForeignKey(Author,
        on_delete = models.CASCADE,
        null      = True
    )
    image      = models.ImageField(
        null      = True
    )
    categories = models.ForeignKey(Category,
        on_delete = models.CASCADE,
        null      = True
    )

    def __unicode__(self):
        return '<Quote {content}, {author}>'.format(
            content = ellipsis(self.content, 30),
            author  = self.author
        )