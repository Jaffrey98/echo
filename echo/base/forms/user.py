from django import forms
from django.contrib.auth import models

class UserForm(forms.ModelForm):
    class Meta:
        model  = models.User
        fields = (
            'first_name', 'last_name', 'email', 'password'
        )